﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Mapping;
using NHibernate.Tool.hbm2ddl;
using System;

namespace AppDllProjectOutSide
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("TestMe");



            var dll = typeof(Application).Assembly;


            var cfg = Fluently.
             Configure().
             Database(MsSqlConfiguration.MsSql7.ConnectionString(c => c.Server("127.0.0.1").Database("AppCore").Username("sa").Password("1qazxsw2"))).
             Mappings(m => m.FluentMappings.AddFromAssembly(dll))
             .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true)).BuildSessionFactory();

           
        }
    }

    public class Application
    {
        public virtual string ApplicationCode { get; set; }
        public virtual string ApplicationHost { get; set; }
        public virtual string ApplicationStatus { get; set; }
        public virtual string ConnectionCode { get; set; }
        public virtual string LogConnectionCode { get; set; }
    }
    public class ApplicationMapping : ClassMap<Application>
    {
        ApplicationMapping()
        {
            Id(x => x.ApplicationCode).Length(20).GeneratedBy.Assigned();
            Map(x => x.ApplicationHost).Length(255);
            Map(x => x.ApplicationStatus).Length(1);
            Map(x => x.ConnectionCode).Length(20);
            Map(x => x.LogConnectionCode).Length(20);
            Schema("dbo");
            Table("Application");
        }
    }
}
