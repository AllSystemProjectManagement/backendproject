﻿using AppCore.Entities.AppManager;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationManager.Entities.DataBaseMapping
{
    public class ApplicationMapping : ClassMap<Application>
    {
        ApplicationMapping()
        {
            Id(x => x.ApplicationCode).Length(20).GeneratedBy.Assigned();
            Map(x => x.ApplicationHost).Length(255);
            Map(x => x.ApplicationStatus).Length(1);
            Map(x => x.ConnectionCode).Length(20);
            Map(x => x.LogConnectionCode).Length(20);
            Schema("dbo");
            Table("Application");
        }
    }
    public class ApplicationDatabaseMapping : ClassMap<ApplicationDatabase>
    {
        ApplicationDatabaseMapping()
        {
            Id(x => x.ConnectionCode).Length(20).GeneratedBy.Assigned();
            Map(x => x.ApplicationCode).Length(20);
            Map(x => x.DbName).Length(255);
            Map(x => x.IsDefault);
            Schema("dbo");
            Table("ApplicationDatabase");
        }
    }
    public class ApplicationFunctionMapping : ClassMap<ApplicationFunction>
    {
        ApplicationFunctionMapping()
        { 
            Map(x => x.IsWaitFunction);
            Map(x => x.IsLog);
            CompositeId().KeyProperty(x => x.ApplicationCode, x => { x.Length(20); }).KeyProperty(x => x.FunctionName, x => { x.Length(255); });
            Schema("dbo");
            Table("ApplicationFunction");
        }
    } 
    public class ApplicationFunctionPermissionMapping : ClassMap<ApplicationFunctionPermission>
    {
        ApplicationFunctionPermissionMapping()
        { 
            Map(x => x.Status);
            CompositeId()
           .KeyProperty(x => x.ApplicationCode, x => { x.Length(20); })
           .KeyProperty(x => x.FunctionName, x => { x.Length(255); })
           .KeyProperty(x => x.PermissionApplicationCode, x => { x.Length(20); });
            Schema("dbo");
            Table("ApplicationFunctionPermission");
        }
    }
     
    public class ConnectionsMapping : ClassMap<Connections>
    {
        ConnectionsMapping()
        {
            Id(x => x.ConnectionCode).Length(20).GeneratedBy.Assigned();
            Map(x => x.ConnectionUser).Length(255);
            Map(x => x.ConnectionPassword).Length(255);
            Map(x => x.ConnectionLink).Length(255);
            Map(x => x.DbType).Length(255); 
            Schema("dbo");
            Table("Connections");
        }
    }
}
