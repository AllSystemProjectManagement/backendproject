﻿using ApplicationManager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationManager.Operation
{
    public static class Startup
    {
        public static BaseStartup Start()
        {
            return new BaseStartup()
            {
                EntitiesProject = "ApplicationManager.Entities",
                DataBaseEntitiesProject = "ApplicationManager.Entities.DataBaseMapping",
                OperationProject = "ApplicationManager.Operation",
            };
        }
    }
}
