﻿using AppCore.Entities.AppManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationManager.Operation.ApplicationManager
{
    public class ApsManager
    {

        protected static List<Application> ApplicationList { get; set; }
        protected static List<ApplicationFunctionPermission> ApplicationFunctionPermission { get; set; }
        protected static List<ApplicationFunction> ApplicationFunction { get; set; }
        protected static List<ApplicationDatabase> ApplicationDatabase { get; set; }
        protected static List<Connections> Connections { get; set; }
        public static ApplicationStartApplicationData GetApplication(string appCode)
        {

            ApplicationStartApplicationData data = new ApplicationStartApplicationData();
            if (ApplicationList == null)
            {
                using (var db = dbContext.OpenSession())
                {
                    ApplicationList = db.Query<Application>().Where(t => t.ApplicationStatus == "A").ToList();
                    ApplicationFunctionPermission = db.Query<ApplicationFunctionPermission>().Where(t => t.Status == "A").AsQueryable().ToList();
                    ApplicationFunction = db.Query<ApplicationFunction>().AsQueryable().ToList();
                    ApplicationDatabase = db.Query<ApplicationDatabase>().AsQueryable().ToList();
                    Connections = db.Query<Connections>().AsQueryable().ToList();
                }
            }
            var permissionCheck = new string[] { "*", appCode };
            /**
             * Hangi uygulamara Erişebilirim
             */
            data.FunctionList = ApplicationFunction.Where(t =>
                ApplicationFunctionPermission.Any(x =>  permissionCheck.Contains(x.PermissionApplicationCode)
                )).ToList();

            /**
             * Hangi uygumalar bana erişebilir
             */
            data.ApplicationFunctionPermission = ApplicationFunctionPermission.Where(t => t.ApplicationCode==appCode).ToList();
            data.App = ApplicationList.Where(t => data.FunctionList.Any(x => x.ApplicationCode == t.ApplicationCode)).ToDictionary(p => p.ApplicationCode);
            data.ApplicationDatabase = ApplicationDatabase.Where(t => t.ApplicationCode == appCode).ToList();
            data.Connections = Connections.Where(t => data.ApplicationDatabase.Any(x=>x.ConnectionCode== t.ConnectionCode)).ToList();

            return data;
        }
    }
}
