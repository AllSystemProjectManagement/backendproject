﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationManager
{
    public class Startup : AppCore.Startup
    {
        public Startup(IConfiguration configuration) : base(configuration)
        {
           BaseStartup= Operation.Startup.Start();
        } 
    }
}
