﻿using AppCore.Entities.AppManager;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json ;

namespace AppCore
{
    public class Startup
    {
        public BaseStartup BaseStartup { get; set; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Tools.ClassGeneration.CCompiler.StartApps(); 
            dbContext.GetAssemlyAnyObject = AppDomain.CurrentDomain.GetAssemblies().Where(t => t.GetName().Name.Contains(BaseStartup.DataBaseEntitiesProject) || BaseStartup.DataBaseEntitiesProject.Contains(t.GetName().Name)).FirstOrDefault();
             
            if (dbContext.GetAssemlyAnyObject==null)
            { 
                throw new Exception(string.Format("Entities dll bulunamadı BaseStartup.DataBaseEntitiesProject : {0}", BaseStartup.DataBaseEntitiesProject));
            }
            services.AddControllers();
            services.AddHttpContextAccessor();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
         
            using (System.IO.StreamReader ConfigReader = new System.IO.StreamReader("AppsConfig.json"))
            {  
                var Confs = ConfigReader.ReadToEnd().unSerialize<ApplicationConfigration>();
                GlobalStatic.Confs = Confs;
                ConfigReader.Close();
                if (Confs == null)
                    throw new Exception("AppsConfig config error");
            }
             
            app.UseStaticHttpContext();

            if (env.IsDevelopment())
            {
             //   app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            { 
                endpoints.MapControllerRoute("ApiSend", "ApiSend");  
            });
        }
    }
}





