﻿using Microsoft.AspNetCore.Mvc;

namespace AppCore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class ApiSend : ControllerBase
    {
        [HttpPut]
        [HttpPost]
        [HttpGet]
        [HttpDelete]
        public IActionResult Index()
        {
            return Ok(Apps.Proxy.ProxyService.WebRun()); 
        }
    }
}
