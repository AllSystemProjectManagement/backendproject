﻿using AppCore.Entities.AppManager;
using System.Collections.Generic;

namespace AppCore.Apps.IProxy
{
    public interface IAppManager
    {  
        public ApplicationStartApplicationData GetApplication(string AppCode);
        public Dictionary<string, Application> GetFunctionList(string AppCode);

    }
}
