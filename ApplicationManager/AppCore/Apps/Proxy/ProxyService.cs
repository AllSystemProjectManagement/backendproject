﻿using AppCore.Entities.AppManager;
using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace AppCore.Apps.Proxy
{
    public static class ProxyService
    {
        internal static ApplicationStartApplicationData ApplicationLink { get; set; }
        internal static Dictionary<string, string> Permission { get; set; }
        public static object RUN(string AppCode, string FunctionName, params object[] param)
        {
            if (ApplicationLink == null)
            {
                LoadApplicationLink(param[0].ToString());
            }
            object returnData = null;
            if (GlobalStatic.Confs.AppName == AppCode)
            {
                var x = AppDomain.CurrentDomain.GetAssemblies();
                if (x.Any(t => t.GetName().Name.Contains(".Base.Host")))
                {
                    var assemly = x.First(t => t.GetName().Name.Contains(".Base.Host")); 
                    returnData = (ApplicationStartApplicationData)RunInvokeAseemly(assemly, AppCode, FunctionName, param); 
                }
            }
            else
            {

            }

            return returnData;
        }

        private static void LoadApplicationLink(string param)
        {
            ApplicationStartApplicationData returnData = null; 
            if(GlobalStatic.Confs.AppName== "AppManager")
            {
                var x=AppDomain.CurrentDomain.GetAssemblies();
                if(x.Any(t=>t.GetName().Name == "ApplicationManager.Base.Host"))
                {
                    var assemly = x.First(t => t.GetName().Name == "ApplicationManager.Base.Host");
                    returnData= (ApplicationStartApplicationData)RunInvokeAseemly(assemly, "AppManager", "GetApplication", param);

                }
            } 
            ApplicationLink = returnData;
        }

        static Dictionary<string,object> Applications { get; set; }
        public static object RunInvokeAseemly(Assembly dll, string AppCode, string FunctionName, params object[] param)
        {
            if (Applications == null)
            {
                Applications = new Dictionary<string, object>();
            }
            object app;
            if (!Applications.ContainsKey(AppCode))
            {
                var type = dll.GetTypes().Where(t => t.Name == AppCode).FirstOrDefault();
                if(type==null)
                {
                    throw new Exception("RunInvokeAseemly " + AppCode);
                }

                app = Activator.CreateInstance(type);
                Applications.Add(AppCode, app);
            }
            var func = Applications[AppCode].GetType().GetMethod(FunctionName);
            var returnData = func.Invoke(Applications[AppCode], param);
            return returnData;
        }
        internal static object WebRun()
        {
            RunApplication app = null;
            if (System.Web.HttpContext.Current.Request.Body.CanRead)
            {
                string documentContents;
                using (Stream receiveStream = System.Web.HttpContext.Current.Request.BodyReader.AsStream())
                {
                    using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                    {
                        documentContents = readStream.ReadToEnd();
                    }
                }
                app = documentContents.unSerialize<RunApplication>();
            }
            Type type = typeof(Aps);
            var prop = type.GetProperty(app.Application);
            object returnData = null;
            if (prop == null)
            {
                throw new Exception("Application Error AppCore Update DLL");
            }
            var func = prop.PropertyType.GetMethod(app.FunctionName);
            if (func == null)
            {
                throw new Exception("Application Error AppCore Update DLL not found function");
            }
            returnData = Apps.Proxy.ProxyService.RUN(app.Application, app.FunctionName, app.Params);
            return returnData;
        }
    }
}
