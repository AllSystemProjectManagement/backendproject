﻿ 
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System;
using System.Linq;

namespace AppCore.Tools.ClassGeneration
{
    public class CCompiler 
    {

        protected static Assembly Assembly { get; set; }
        protected static void LoadAssemly(string code, List<string> refusings)
        { 
            var tree = SyntaxFactory.ParseSyntaxTree(code);
            var systemRefLocation = typeof(object).GetTypeInfo().Assembly.Location;

            var systemReference = refusings.Select(t => MetadataReference.CreateFromFile(t)).ToArray();
           

             
            var compilation = CSharpCompilation.Create("RuntimeProxy.dll")
              .WithOptions(
                new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
              .AddReferences(systemReference)
              .AddSyntaxTrees(tree);
            MemoryStream stream = new MemoryStream();
            EmitResult compilationResult = compilation.Emit(stream);
            stream.Flush();
            if (compilationResult.Success)
            { 
                Assembly asm = Assembly.Load(stream.ToArray());
                Assembly = asm;
            }else
            {
                throw new Exception("Not Load Proxy\n"+ string.Join(",", compilationResult.Diagnostics.Select(t => t.ToString())));
            }
        }

        internal static void StartLoadCode(Dictionary<string, Type> types)
        {
            List<string> classes = new List<string>();
            List<string> usings = new List<string>();
            List<string> refusings = new List<string>();
            foreach (var item in types)
            {
                classes.Add(GenerationClass(item.Value,usings, refusings));
                usings.Add(item.Value.Namespace);
                refusings.Add(item.Value.Assembly.Location);
            }
            var usingsrefs = usings;
            usings = usings.Distinct().Select(t => string.Format("using {0};\n", t).ToString()).ToList();


            string Code = string.Format("{0}\n\n namespace AppCore.Apps.IProxy #1#\n\n{1}\n\n#2# ", string.Join("", usings), string.Join("", classes)).Replace("#1#", "{ ").Replace("#2#", " }");

            refusings = refusings.Distinct().ToList();

            var files = Directory.GetFiles(Path.GetDirectoryName(typeof(Dictionary<string, string>).Assembly.Location), "system*.dll").ToList();
            refusings.AddRange(files);

            LoadAssemly(Code, refusings);

        }

        internal static string GenerationClass(Type items, List<string> usings, List<string> refusings)
        {
            string Class = @"
public class {0} : {1} #1# 
    {2}
#2#";

            //{0} donus tipi , {1} funcname  ,{2} params,{3} return {4} return type ,{5} AppName,{6} function name,{7} paramsName
            string func = @"
    public {0} {1}({2})
    #1#
        {3} {4}Proxy.ProxyService.RUN(""{5}"", ""{6}"", {7});
    #2#";


            List<string> funcs = new List<string>();

            foreach (var item in items.GetMethods())
            {
                string methodType = "void";
                string returnType = "";
                string returnKey = "";
                if(item.ReturnType.Name.ToLower()!="void")
                {
                    returnKey = "return";
                    methodType = getTypeName(item.ReturnType, usings, refusings); 
                    returnType = string.Format("({0})", methodType); 
                }
                string inputs = "";
                string inputvar = "null";
                if (item.GetParameters().Length>0)
                {
                    inputs = string.Join(", ",  item.GetParameters().Select(t => getTypeName(t.ParameterType, usings, refusings) + " " + t.Name).ToArray());
                    inputvar = string.Join(", ",  item.GetParameters().Select(t =>  t.Name).ToArray());
                }


                string methodName = item.Name;

                string fnc = string.Format(func,methodType,methodName,inputs,returnKey,returnType,items.Name.Substring(1),methodName,inputvar).Replace("#1#", "{ ").Replace("#2#", " }");

                funcs.Add(fnc); 
            }

            Class=string.Format(Class, items.Name.Substring(1), items.Name,string.Join("\n", funcs)).Replace("#1#", "{ ").Replace("#2#", " }");

            return Class;
        }
        internal static string getTypeName(Type type, List<string> usings, List<string> refusings)
        {
             refusings.Add(type.GetTypeInfo().Assembly.Location);
            string typeName = "";
            usings.Add(type.Namespace);
            if(type.IsGenericType)
            {
                
                typeName = type.Name.Substring(0,type.Name.IndexOf('`'));
                typeName = string.Format("{0}<{1}>", typeName, string.Join(',', type.GenericTypeArguments.Select(t => getTypeName(t, usings,refusings)).ToArray()));
            }
            else
            {
                typeName = type.Name; 
            }
            return typeName;
        }
        internal static void StartApps()
        {
            var type = typeof(Aps);

            Dictionary<string, Type> types = new Dictionary<string, Type>();
            foreach (var item in type.GetProperties())
            {
                types.Add(item.Name, item.PropertyType);
            }
            StartLoadCode(types);

            foreach (var item in type.GetProperties())
            {
                string Creator =string.Format("AppCore.Apps.IProxy.{0}", item.PropertyType.Name.Substring(1));

                var Itype = Assembly.GetType(Creator);

                var proxy=Activator.CreateInstance(Itype); 
                
                item.SetValue(null,proxy);
            } 
        }
    }
}
