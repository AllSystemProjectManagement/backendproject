﻿using FluentNHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System; 
using System.Linq;
using NHibernate.Tool.hbm2ddl;
using System.Reflection;
using AppCore;

public class dbContext
{
    //private static ISessionFactory _sessionFactory;
    private static  System.Collections.Generic.Dictionary<string, ISessionFactory>  _sessionFactory=new System.Collections.Generic.Dictionary<string, ISessionFactory>();
    internal static Assembly GetAssemlyAnyObject { get; set; }
    protected static string DbDefaultName { get; set; }
    public static ISession OpenSession()
    {
        if (string.IsNullOrEmpty(DbDefaultName))
        { 
            var database = GlobalStatic.Confs.DataBase.FirstOrDefault(t => t.IsDefault == true);
            DbDefaultName = database.DbName;
        }
        return OpenSession(DbDefaultName);
    }
    public static ISession OpenSession(string dbName)
    {
        if (!_sessionFactory.ContainsKey(dbName))
        {
            var database = GlobalStatic.Confs.DataBase.FirstOrDefault(t => t.IsDefault == true);
            if(database==null)
            { 
                  database = GlobalStatic.Confs.DataBase.FirstOrDefault(t => t.DbName == dbName);
            }else
            {
                DbDefaultName = database.DbName;
            }
            dbName = database.DbName;
            if (database == null)
            {
                throw new Exception(string.Format("dbContext GlobalStatic.Confs.DataBase  notFound DataBaseName {0}", database));
            }
            if (!GlobalStatic.Confs.DataBaseConnection.ContainsKey(database.ConnectionCode))
            {
                throw new Exception(string.Format("dbContext GlobalStatic.Confs.DataBase notFound {0}", database.ConnectionCode));
            }
            var connection = GlobalStatic.Confs.DataBaseConnection[database.ConnectionCode];

            var cfg = Fluently.
             Configure().
             Database(MsSqlConfiguration.MsSql7.ConnectionString(c => c.Server(connection.ConnectionLink).Database(database.DbName).Username(connection.ConnectionUser).Password(connection.ConnectionPassword))).
             Mappings(m => m.FluentMappings.AddFromAssembly(GetAssemlyAnyObject))
             .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true)).BuildSessionFactory();
           /* var cfg = Fluently.
                Configure().
                Database(MsSqlConfiguration.MsSql7.ConnectionString(c => c.Server(connection.ConnectionLink).Database(database.DbName).Username(connection.ConnectionUser).Password(connection.ConnectionPassword))).
                Mappings(m => m.FluentMappings.AddFromAssembly(GetAssemlyAnyObject))
                .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true));
            BuildSchema(cfg);*/
            _sessionFactory.Add(dbName, cfg);
        }
        return _sessionFactory[dbName].OpenSession();
    }

    protected static void BuildSchema(FluentConfiguration configuration)
    {
        var sessionSource = new SessionSource(configuration);
        var session = sessionSource.CreateSession();
        sessionSource.BuildSchema(session);
    }
} 