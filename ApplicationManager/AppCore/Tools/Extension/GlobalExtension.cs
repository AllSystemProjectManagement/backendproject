﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

public static class GlobalExtension
{

    public static int GCC { get; internal set; }

    /// <summary>
    /// Object  IS NUMBER 
    /// </summary>
    /// <param name="s"></param>
    /// <returns>bool</returns>
    public static bool IsNumeric(this object s)
    {
        float output;
        return float.TryParse(s.ToString(), out output);
    }
    /// <summary>
    /// JSON VALUE IS NULL
    /// </summary> 
    /// <returns></returns>
    public static bool IsNullJson(this JToken s)
    {
        return s.Type == JTokenType.Null;
    }
    /// <summary>
    /// Object Is bool? return
    /// </summary>
    /// <param name="s">object data</param>
    /// <param name="def">return default value</param>
    /// <returns></returns>
    public static bool? BoolNullValue(this object s, bool? def = null)
    {
        bool ret = false;
        if (bool.TryParse(s.toNullStringValue(), out ret))
        {
            return ret;
        }
        else
        {
            return def;
        }
    }
    /// <summary>
    /// Json value is string 
    /// </summary>
    /// <param name="s">json data value</param>
    /// <returns>string</returns>
    public static object IsNullValueString(this JToken s)
    {
        return s.Type != JTokenType.Null ? s.ToString() : null;
    }
    /// <summary>
    /// string is converted JObject
    /// </summary>
    /// <param name="s">string data</param>
    /// <returns>JObject</returns>
    public static JObject GetJObject(this string s)
    {
        return JObject.Parse(s);
    }
    /// <summary>
    /// string is converted JArray
    /// </summary>
    /// <param name="s">string data</param>
    /// <returns>JArray</returns>
    public static JArray GetJArray(this string s)
    {
        return JArray.Parse(s);
    }

    /// <summary>
    /// datetime is converted string format date
    /// </summary>
    /// <param name="s">datetime data</param>
    /// <returns>string</returns>
    public static string GetFormatViewDate(this DateTime time, ViewDateFormat viewDateFormat = ViewDateFormat.Date)
    {
        return GetFormatViewDate((DateTime?)time, viewDateFormat);
    }
    /// <summary>
    /// datetime is converted string format date
    /// </summary>
    /// <param name="s">datetime? data</param>
    /// <returns>string</returns>
    public static string GetFormatViewDate(this DateTime? time, ViewDateFormat viewDateFormat = ViewDateFormat.Date)
    {
        if (time == null)
            return "";
        switch (viewDateFormat)
        {
            case ViewDateFormat.DateTime:
                return ((DateTime)time).ToString("dd.MM.yyyy hh:mm");
            case ViewDateFormat.Time:

                return ((DateTime)time).ToString("hh:mm");
            case ViewDateFormat.Date:

                return ((DateTime)time).ToString("dd.MM.yyyy");
        }
        return "";
    }
    /// <summary>
    /// object is SerializeObject string format data
    /// </summary>
    /// <param name="s">object data</param>
    /// <returns>string</returns>
    public static string toSerialize(this object o)
    {
        return JsonConvert.SerializeObject(o);
    }
    /// <summary>
    /// string is Null or Convert to String 
    /// </summary>
    /// <param name="s">object data</param>
    /// <returns>string</returns>
    public static string toNullStringValue(this object o)
    {

        if (o != null && (o.GetType().Name == "JToken" || o.GetType().Name == "JValue"))
        {
            return ((JToken)o).Type == JTokenType.Null ? null : o.ToString();
        }
        return o == null ? null : o.ToString();
    }

    /// <summary>
    /// string is JavaScriptStringEncode
    /// System.Web.HttpUtility.JavaScriptStringEncode
    /// </summary>
    /// <param name="s">string data</param>
    /// <returns>string</returns>
    public static string toJavaScriptValue(this string o)
    {

        return System.Web.HttpUtility.JavaScriptStringEncode(o, true);

    }
    /// <summary>
    /// object is converted data nulable decimal? 
    /// </summary>
    /// <param name="s">object data</param>
    /// <returns>decimal?</returns>
    public static decimal? toNullGetDecimalValue(this object o)
    {
        if (o == null || o.IsNumeric() == false)
            return null;
        return decimal.Parse(o.ToString());
    }
    /// <summary>
    /// string is converted Object
    /// </summary>
    /// <param name="s">string data</param>
    /// <returns>object</returns>
    public static object deSerialize(this string o)
    {
        return JsonConvert.DeserializeObject(o);
    }
    /// <summary>
    /// object[] is converted JArray
    /// </summary>
    /// <param name="s">object[] data</param>
    /// <returns>JArray</returns>
    public static Newtonsoft.Json.Linq.JArray getJsonArray(this object[] s)
    {
        return s.toSerialize().unSerialize<Newtonsoft.Json.Linq.JArray>();
    }
    /// <summary>
    /// object[] is converted JArray
    /// </summary>
    /// <param name="s">List<T> data</param>
    /// <returns>JArray</returns>
    public static Newtonsoft.Json.Linq.JArray getJsonArray<T>(this List<T> s)
    {
        return s.getJsonArray(); //s.toSerialize().unSerialize<Newtonsoft.Json.Linq.JArray>();
    }
    /// <summary>
    /// string is converted Class Type
    /// </summary>
    /// <param name="s">string data</param>
    /// <returns>T</returns>
    public static T unSerialize<T>(this string o)
    {
        try
        {

            T sonuc = JsonConvert.DeserializeObject<T>(o);
            return sonuc;
        }
        catch
        {

            return default(T);
        }
    }
    /// <summary>
    /// string is converted Object variable
    /// </summary>
    /// <param name="s">string data</param>
    /// <returns>Object</returns>
    public static object deSerializeJson(this string o)
    {
        return JsonConvert.DeserializeObject(o, new Newtonsoft.Json.JsonSerializerSettings
        {
            //  TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects,
            StringEscapeHandling = Newtonsoft.Json.StringEscapeHandling.EscapeHtml
        });
    }

    /// <summary>
    /// string is converted T Class Type variable
    /// </summary>
    /// <param name="s">string data</param>
    /// <returns>T</returns>
    public static T unSerializeJson<T>(this string o)
    {
        T sonuc = JsonConvert.DeserializeObject<T>(o, new Newtonsoft.Json.JsonSerializerSettings
        {
            //  TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects,
            StringEscapeHandling = Newtonsoft.Json.StringEscapeHandling.EscapeHtml
        });
        return sonuc;
    }
    /// <summary>
    /// Object is converted string
    /// </summary>
    /// <param name="s">object data</param>
    /// <returns>string</returns>
    public static string toSerializeJson(this object o)
    {

        return JsonConvert.SerializeObject(o, Newtonsoft.Json.Formatting.None, new Newtonsoft.Json.JsonSerializerSettings
        {
            //TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects,
            StringEscapeHandling = Newtonsoft.Json.StringEscapeHandling.EscapeHtml
        });
    }
    /// <summary>
    /// HttpUtility.UrlEncodeUnicode
    /// </summary>
    /// <param name="link">string</param>
    /// <returns>string</returns>
    public static string urlEncode(this string link)
    {
        return HttpUtility.UrlEncodeUnicode(link);
    }
    /// <summary>
    /// HttpUtility.UrlDecode
    /// </summary>
    /// <param name="link">string</param>
    /// <returns>string</returns>
    public static string urlDecode(this string link)
    {
        return HttpUtility.UrlDecode(link);
    }
    /// <summary>
    /// object type Is Numbers
    /// </summary>
    /// <param name="dataType">Type GetType()</param>
    /// <returns>bool</returns>
    public static bool IsNumbers(this Type dataType)
    {
        if (dataType == null)
            throw new ArgumentNullException("dataType is null");

        return (dataType == typeof(int)
            || dataType == typeof(double)
            || dataType == typeof(long)
            || dataType == typeof(short)
            || dataType == typeof(float)
            || dataType == typeof(Int16)
            || dataType == typeof(Int32)
            || dataType == typeof(Int64)
            || dataType == typeof(uint)
            || dataType == typeof(UInt16)
            || dataType == typeof(UInt32)
            || dataType == typeof(UInt64)
            || dataType == typeof(sbyte)
            || dataType == typeof(Single));
    }
    /// <summary>
    /// object type is DateTime ?
    /// </summary>
    /// <param name="dataType">Type GetType()</param>
    /// <returns>bool</returns>
    public static bool IsDateTime(this Type dataType)
    {
        if (dataType == null)
            throw new ArgumentNullException("dataType is null");

        return (dataType == typeof(DateTime));
    }

    public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
    {
        foreach (T item in enumeration)
        {
            action(item);
        }
    }
}

public enum ViewDateFormat
{
    Date = 1,
    DateTime = 2,
    Time = 3
}

#region static httpcontext
internal static class StaticHttpContextExtensions
{
    public static void AddHttpContextAccessor(this IServiceCollection services)
    {
        services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
    }

    public static IApplicationBuilder UseStaticHttpContext(this IApplicationBuilder app)
    {
        var httpContextAccessor = app.ApplicationServices.GetRequiredService<IHttpContextAccessor>();
        System.Web.HttpContext.Configure(httpContextAccessor);
        return app;
    }
}

namespace System.Web
{
    public static class HttpContext
    {
        private static IHttpContextAccessor _contextAccessor;

        public static Microsoft.AspNetCore.Http.HttpContext Current => _contextAccessor.HttpContext;

        internal static void Configure(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }
    }
}

#endregion
