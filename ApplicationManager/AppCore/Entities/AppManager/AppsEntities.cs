﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppCore.Entities.AppManager
{
    public class BaseStartup
    {
        public string EntitiesProject { get; set; }
        public string OperationProject { get; set; }
        public string DataBaseEntitiesProject { get; set; }
    }
    public class ApplicationConfigration
    {
        public string AppName { get; set; }
        public Dictionary<string, string> Aps { get; set; }
        public Dictionary<string, Connections> DataBaseConnection { internal get; set; }
        public List<ApplicationDatabase> DataBase { internal get; set; }
    }

    internal class RunApplication
    {
        public string Application { get; set; }
        public string FunctionName { get; set; }
        public object[] Params { get; set; }
    }

    public class Application
    {
        public virtual string ApplicationCode { get; set; }
        public virtual string ApplicationHost { get; set; }
        public virtual string ApplicationStatus { get; set; }
        public virtual string ConnectionCode { get; set; }
        public virtual string LogConnectionCode { get; set; }
    }


    public class ApplicationDatabase
    {
        public virtual string ConnectionCode { get; set; }
        public virtual string ApplicationCode { get; set; }
        public virtual string DbName { get; set; }
        public virtual bool IsDefault { get; set; }
    }

    public class ApplicationFunction
    {
        public virtual string ApplicationCode { get; set; }
        public virtual string FunctionName { get; set; }
        public virtual bool IsWaitFunction { get; set; }
        public virtual bool IsLog { get; set; }

        public override int GetHashCode()
        {
            return (ApplicationCode + "|" + FunctionName).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            var t = obj as ApplicationFunction;

            if (t == null)
            {
                return false;
            }

            return this.ApplicationCode == t.ApplicationCode && this.FunctionName == t.FunctionName;
        }
    }


    public class ApplicationStartApplicationData
    {
        public Dictionary<string, Application> App { set; get; }
        public List<ApplicationFunction> FunctionList { get; set; }
        public List<ApplicationFunctionPermission> ApplicationFunctionPermission { get; set; }
        public List<ApplicationDatabase> ApplicationDatabase { get; set; }
        public List<Connections> Connections { get; set; }
    }

    public class ApplicationFunctionPermission
    {
        public virtual string ApplicationCode { get; set; }
        public virtual string FunctionName { get; set; }
        public virtual string PermissionApplicationCode { get; set; }
        public virtual string Status { get; set; }

        public override int GetHashCode()
        {
            return (ApplicationCode + "|" + FunctionName + "|" + PermissionApplicationCode).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            var t = obj as ApplicationFunctionPermission;

            if (t == null)
            {
                return false;
            }

            return this.ApplicationCode == t.ApplicationCode && this.FunctionName == t.FunctionName && this.PermissionApplicationCode == t.PermissionApplicationCode;
        }
    }

    public class Connections
    {
        public virtual string ConnectionCode { get; set; }
        public virtual string ConnectionUser { get; set; }
        public virtual string ConnectionPassword { get; set; }
        public virtual string ConnectionLink { get; set; }
        public virtual string DbType { get; set; }
    } 
}
